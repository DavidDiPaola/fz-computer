#include "lib/z80/z80.h"

#include <inttypes.h>

#include <SDL.h>

#include <SDL_framerate.h>

#include <stdlib.h>

#include <stdio.h>

#include <unistd.h>

#include "emu.h"

#define _sdl_error(SDL_FUNCTION_NAME) \
	fprintf(stderr, "ERROR: %s() at %s:%s():%i -- %s" "\n", (SDL_FUNCTION_NAME), __FILE__, __func__, __LINE__, SDL_GetError());

static struct {
	SDL_Surface * screen;
	FPSmanager fpsManager;
} _sdl;

static void
_file_readAll(const char * path, uint8_t * * out_data, size_t * out_data_size) {
	FILE * file = fopen(path, "r");
	if (!file) {
		perror(path);
		exit(1);
	}

	fseek(file, 0, SEEK_END);
	size_t data_size = ftell(file);
	fseek(file, 0, SEEK_SET);

	uint8_t * data = malloc(data_size);
	if (!data) {
		perror(NULL);
		exit(1);
	}

	size_t fread_amount = fread(data, 1, data_size, file);
	if (fread_amount != data_size) {
		fprintf(stderr, "ERROR: tried to read %zi bytes from \"%s\" but only got %zi bytes" "\n", data_size, path, fread_amount);
		exit(1);
	}

	int status = fclose(file);
	if (status != 0) {
		perror(path);
		exit(1);
	}

	*out_data      = data;
	*out_data_size = data_size;
}


static void
_init(const char * bios_PATH) {
	int status;

	status = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_JOYSTICK);
	if (status < 0) {
		_sdl_error("SDL_Init");
		exit(1);
	}

	_sdl.screen = SDL_SetVideoMode(256, 240, /*bpp=*/8, /*flags=*/SDL_DOUBLEBUF);
	if (!_sdl.screen) {
		_sdl_error("SDL_SetVideoMode");
		exit(1);
	}

	SDL_initFramerate(&_sdl.fpsManager);

	status = SDL_setFramerate(&_sdl.fpsManager, /*framerate=*/60);
	if (status < 0) {
		_sdl_error("SDL_setFramerate");
		exit(1);
	}

	uint8_t * bios;
	size_t    bios_size;
	_file_readAll(bios_PATH, &bios, &bios_size);
	emu_init(bios, bios_size);
	free(bios);
}

static void
_halt(void) {
	SDL_Quit();
}

static void
_quit(unsigned int status) {
	_halt();
	exit(status);
}

static void
_waitForKey(void) {
	bool done = 0;
	while (!done) {
		SDL_Event event;
		int status = SDL_WaitEvent(&event);
		if (status == 0) {
			_sdl_error("SDL_WaitEvent()");
		}
		switch (event.type) {
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym) {
					case SDLK_ESCAPE:
						_quit(0);
					default:
						break;
				}
				done = 1;
				break;
			case SDL_QUIT:
				_quit(0);
			default:
				break;
		}

		if (done) {
			break;
		}
	}
}

bool         _arg_paused   = 0;
const char * _arg_biospath = NULL;

void
_arg_parse(int argc, char * argv[]) {
	int opt;
	while ((opt = getopt(argc, argv, ":p")) >= 0) {
		switch (opt) {
			case 'p':
				_arg_paused = 1;
				break;
			case ':':
				fprintf(stderr, "ERROR: flag \"%s\" needs an argument" "\n", argv[optind-1]);
				goto syntax;
				break;
			case '?':
				fprintf(stderr, "ERROR: unknown flag \"%s\"" "\n",  argv[optind-1]);
				goto syntax;
				break;
		}
	}
	if (optind >= (argc - 1)) {
		goto syntax;
	}
	_arg_biospath = argv[optind + 0];
	goto done;

	syntax:
	fprintf(stderr, "Syntax: %s <flags> <bios>" "\n", argv[0]);

	fprintf(stderr, "Flags:" "\n");
	fprintf(stderr, "\t" "-p -- start the emulator paused" "\n");
	exit(1);

	done:
	return;
}

int
main(int argc, char * argv[]) {
	_arg_parse(argc, argv);

	_init(_arg_biospath);

	emu_dump_mem(0x0000, 512);
	emu_dump_cpu();

	printf("====RUN================================" "\n");
	for (long i=0; i<100000l; i++) {
		emu_step();

		if (_arg_paused) {
			emu_dump();
			_waitForKey();
		}
	}
	printf("\n" "====DONE===============================" "\n");

	_halt();

	return 0;
}
