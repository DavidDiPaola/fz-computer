/* 2022 David DiPaola, licensed CC0 (public domain worldwide) */

/* event handlers (see [1] page 6) */
.org 0x0000  /* reset handler */
	jp init
.org 0x0066  /* NMI handler */
	jp init  /* TODO actually handle this */

.org 0x0100  /* TODO figure out where code should go */

init:
	/* set SP to the top of un-banked RAM */
	ld sp, 0x7FFE

	/* set RAM bank to 0x008000-0x00FFFF */
	ld a, 0x01
	out (0b00000000), a

	call uart_init

	/*
	ld d, '?'
	call uart_write_char
	*/
	ld hl, message
	call uart_write_str

	jp .

message:
	.string "Hello world!"

message2:
	.string "ABCDEFGHIJKLMNOPQRSTUVWXYZ"



.equ char_NUL, 0x00

.equ uart_DATA, 0b00100000
.equ uart_LCR,  0b00100011
.equ uart_LSR,  0b00100101
.equ uart_LSR_THREMPTY, 0b00100000
.equ uart_DLL,  0b00100000
.equ uart_DLM,  0b00100001
.equ uart_baud_9600, 0x000C

uart_init:
	push de

	ld de, uart_baud_9600
	call uart_baud_set

	pop de
	ret

uart_baud_set:
	push af

	/* turn on divisor latch access */
	ld a, 0x80
	out (uart_LCR), a

	/* set divisor LSB */
	ld a, e
	out (uart_DLL), a

	/* set divisor MSB */
	ld a, d
	out (uart_DLM), a

	/* turn off divisor latch access */
	ld a, 0x00
	out (uart_LCR), a

	pop af
	ret

/*
Write a character via the serial port.
	d - the character to be sent.
*/
uart_write_char:
	push af

	/* wait until TX is not full */
1:	in a, (uart_LSR)
	and ~uart_LSR_THREMPTY
	jp z, 1b

	/* push char to UART TX FIFO */
	ld a, d
	out (uart_DATA), a
	
	pop af
	ret

/*
Write a string via the serial port.
	hl - the address of the string to be sent.
*/
uart_write_str:
	push af
	push hl

	/* wait until TX is not full */
1:	in a, (uart_LSR)
	and ~uart_LSR_THREMPTY
	jp z, 1b

	/* output char */
	ld a, (hl)           /* get character from string */
	cp char_NUL          /* is it NUL? */
	jp z, 2f             /* if so, quit */
	out (uart_DATA), a   /* push char to UART TX FIFO */
	inc hl               /* point to next character */
	jp 1b

2:	pop hl
	pop af
	ret

/*
References
	[1] Z80 CPU User Manual UM008011-0816 Revision 11
*/
