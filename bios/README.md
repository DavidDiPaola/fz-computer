# fz_bios
FZ Computer BIOS

## Build
1. Install GNU Make and Z80 GNU Binutils (Ubuntu/Debian: `sudo apt install make binutils-z80`)
2. `make`
3. `bios.bin` is the flat binary file for the flash ROM
